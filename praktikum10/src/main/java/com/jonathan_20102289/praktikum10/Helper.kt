package com.jonathan_20102289.praktikum10

import android.database.Cursor
import com.jonathan_20102289.praktikum10.data.Quote
import com.jonathan_20102289.praktikum10.db.DatabaseContract
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

object Helper {
    var categoryList = arrayOf(
        "Motivasi",
        "Persahabatan",
        "Percintaan",
        "Galau",
        "Keluarga",
        "Musik",
        "Film"
    )
    const val EXTRA_QUOTE = "extra_quote"
    const val EXTRA_POSITION = "extra_position"
    const val RESULT_ADD = 101
    const val RESULT_UPDATE = 201
    const val RESULT_DELETE = 301
    const val ALERT_DIALOG_CLOSE = 10
    const val ALERT_DIALOG_DELETE = 20
    fun mapCursorToArrayList(notesCursor: Cursor?): ArrayList<Quote> {
        val quotesList = ArrayList<Quote>()
        notesCursor?.apply {
            while (moveToNext()) {
                val id =
                    getInt(getColumnIndexOrThrow(DatabaseContract.QuoteColumns._ID))
                val date =
                    getString(getColumnIndexOrThrow(DatabaseContract.QuoteColumns.DATE))
                val title =
                    getString(getColumnIndexOrThrow(DatabaseContract.QuoteColumns.TITLE))
                val description =
                    getString(getColumnIndexOrThrow(DatabaseContract.QuoteColumns.DESCRIPTION))
                val category =
                    getString(getColumnIndexOrThrow(DatabaseContract.QuoteColumns.CATEGORY))
                val author =
                    getString(getColumnIndexOrThrow(DatabaseContract.QuoteColumns.AUTHOR))
                val publish =
                    getString(getColumnIndexOrThrow(DatabaseContract.QuoteColumns.PUBLISH))
                quotesList.add(Quote(id, date, title, description, category, author, publish))
            }
        }
        return quotesList
    }

    fun getCurrentDate(): String {
        val dateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault())
        val date = Date()
        return dateFormat.format(date)
    }
}
