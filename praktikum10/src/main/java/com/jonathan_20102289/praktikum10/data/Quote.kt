package com.jonathan_20102289.praktikum10.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Quote(
    var id: Int = 0,
    var date: String? = null,
    var title: String? = null,
    var description: String? = null,
    var category: String? = null,
    var author: String? = null,
    var publish: String? = null
) : Parcelable

