package com.jonathan_20102289.praktikumm14

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.jonathan_20102289.praktikumm14.Helper.EXTRA_POSITION
import com.jonathan_20102289.praktikumm14.Helper.EXTRA_QUOTE
import com.jonathan_20102289.praktikumm14.Helper.REQUEST_UPDATE
import com.jonathan_20102289.praktikumm14.databinding.ItemQuoteBinding
import com.jonathan_20102289.praktikumm14.model.Quote

class QuoteAdapter(private val activity: Activity) : RecyclerView.Adapter<QuoteAdapter.QuoteViewHolder>() {

    private val callback = object : DiffUtil.ItemCallback<Quote>() {
        override fun areItemsTheSame(oldItem: Quote, newItem: Quote): Boolean =
            oldItem.quote_id == newItem.quote_id

        override fun areContentsTheSame(oldItem: Quote, newItem: Quote): Boolean =
            oldItem == newItem
    }

    val differ = AsyncListDiffer(this, callback)

    inner class QuoteViewHolder(private val binding: ItemQuoteBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(quote: Quote) {
            with(binding) {
                tvItemTitle.text = quote.quote_name
                tvOwner.text = itemView.context.getString(
                    R.string.owner_data,
                    quote.user_name,
                    quote.class_name
                )
                tvItemDate.text = quote.created_at
                tvItemDescription.text = quote.quote_description
                cvItemQuote.setOnClickListener{
                    val intent = Intent(activity, QuoteAddUpdateActivity::class.java)
                    intent.putExtra(EXTRA_POSITION, adapterPosition)
                    intent.putExtra(EXTRA_QUOTE, quote)
                    activity.startActivityForResult(intent, REQUEST_UPDATE)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuoteViewHolder {
        val binding = ItemQuoteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return QuoteViewHolder(binding)
    }

    override fun getItemCount(): Int = differ.currentList.size

    override fun onBindViewHolder(holder: QuoteViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }
}