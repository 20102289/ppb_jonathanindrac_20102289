package com.jonathan_20102289.praktikumm14.model

data class QuoteResponse(
    val quotes: ArrayList<Quote>)