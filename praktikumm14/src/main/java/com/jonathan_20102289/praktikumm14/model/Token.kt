package com.jonathan_20102289.praktikumm14.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Token (
    var token: String? = null
): Parcelable