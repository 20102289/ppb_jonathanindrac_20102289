package com.jonathan_20102289.praktikumm14.ui.myquotes

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.jonathan_20102289.praktikumm14.CoroutineContextProvider
import com.jonathan_20102289.praktikumm14.Helper.RESULT_ADD
import com.jonathan_20102289.praktikumm14.QuoteAdapter
import com.jonathan_20102289.praktikumm14.QuoteAddUpdateActivity
import com.jonathan_20102289.praktikumm14.TokenPref
import com.jonathan_20102289.praktikumm14.api.MainPresenter
import com.jonathan_20102289.praktikumm14.databinding.FragmentMyQuotesBinding

import com.jonathan_20102289.praktikumm14.interfacee.MainView
import com.jonathan_20102289.praktikumm14.model.Login
import com.jonathan_20102289.praktikumm14.model.Quote
import com.jonathan_20102289.praktikumm14.model.Token

class MyQuotesFragment : Fragment(), MainView {

    private var _binding: FragmentMyQuotesBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var presenter: MainPresenter
    private lateinit var adapter: QuoteAdapter
    private lateinit var tokenPref: TokenPref
    private lateinit var token: Token

    private val resultLauncher: ActivityResultLauncher<Intent> =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.data != null) {
                when (it.resultCode) {
                    RESULT_ADD -> {
                        Toast.makeText(context, "Satu item berhasil ditambahkan", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMyQuotesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            recyclerviewMyQuotes.layoutManager = LinearLayoutManager(activity)
            tokenPref = TokenPref(requireActivity())
            token = tokenPref.getToken()
            adapter = QuoteAdapter(requireActivity())
            recyclerviewMyQuotes.adapter = adapter
            presenter = MainPresenter(this@MyQuotesFragment, CoroutineContextProvider())
            progressbar.visibility = View.VISIBLE
            presenter.getMyQuotes(token.token)
            swiperefresh.setOnRefreshListener {
                progressbar.visibility = View.INVISIBLE
                presenter.getMyQuotes(token.token)
            }

            fab.setOnClickListener {
                val intent = Intent(requireActivity(), QuoteAddUpdateActivity::class.java)
                resultLauncher.launch(intent)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getMyQuotes(token.token)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun showMessage(message: String) {
        Toast.makeText(requireActivity(), message, Toast.LENGTH_SHORT).show()
    }

    override fun resultQuote(data: ArrayList<Quote>) {
        adapter.differ.submitList(data)
        binding.apply {
            progressbar.visibility = View.INVISIBLE
            swiperefresh.isRefreshing = false
        }
    }

    override fun resultLogin(data: Login) {}
}