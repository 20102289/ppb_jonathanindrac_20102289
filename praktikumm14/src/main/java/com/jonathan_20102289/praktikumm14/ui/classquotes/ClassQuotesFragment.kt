package com.jonathan_20102289.praktikumm14.ui.classquotes


import android.media.session.MediaSession
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jonathan_20102289.praktikumm14.CoroutineContextProvider
import com.jonathan_20102289.praktikumm14.QuoteAdapter
import com.jonathan_20102289.praktikumm14.TokenPref
import com.jonathan_20102289.praktikumm14.api.MainPresenter
import com.jonathan_20102289.praktikumm14.databinding.FragmentClassQuotesBinding
import com.jonathan_20102289.praktikumm14.interfacee.MainView
import com.jonathan_20102289.praktikumm14.model.Login
import com.jonathan_20102289.praktikumm14.model.Quote
import android.widget.Toast
import com.jonathan_20102289.praktikumm14.model.Token

class ClassQuotesFragment : Fragment(), MainView {

    private var _binding: FragmentClassQuotesBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var presenter: MainPresenter
    private lateinit var adapter: QuoteAdapter
    private lateinit var tokenPref: TokenPref
    private lateinit var token: Token

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentClassQuotesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            recyclerviewClassQuotes.layoutManager = LinearLayoutManager(activity)
            tokenPref = TokenPref(requireActivity())
            token = tokenPref.getToken()
            adapter = QuoteAdapter(requireActivity())
            recyclerviewClassQuotes.adapter = adapter
            presenter = MainPresenter(this@ClassQuotesFragment, CoroutineContextProvider())
            progressbar.visibility = View.VISIBLE
            presenter.getClassQuote(token.token)
            swiperefresh.setOnRefreshListener {
                progressbar.visibility = View.INVISIBLE
                presenter.getClassQuote(token.token)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getClassQuote(token.token)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun showMessage(message: String) {
        Toast.makeText(requireActivity(), message, Toast.LENGTH_SHORT).show()
    }

    override fun resultQuote(data: ArrayList<Quote>) {
        adapter.differ.submitList(data)
        binding.apply {
            progressbar.visibility = View.INVISIBLE
            swiperefresh.isRefreshing = false
        }
    }

    override fun resultLogin(data: Login) {}
}