package com.jonathan_20102289.praktikumm14.ui.globalquotes

import android.media.session.MediaSession
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jonathan_20102289.praktikumm14.CoroutineContextProvider
import com.jonathan_20102289.praktikumm14.QuoteAdapter
import com.jonathan_20102289.praktikumm14.TokenPref
import com.jonathan_20102289.praktikumm14.api.MainPresenter
import com.jonathan_20102289.praktikumm14.databinding.FragmentGlobalQuotesBinding
import com.jonathan_20102289.praktikumm14.interfacee.MainView
import com.jonathan_20102289.praktikumm14.model.Login
import com.jonathan_20102289.praktikumm14.model.Quote
import com.jonathan_20102289.praktikumm14.model.Token

class GlobalQuotesFragment : Fragment(), MainView {

    private var _binding: FragmentGlobalQuotesBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var presenter: MainPresenter
    private lateinit var adapter: QuoteAdapter
    private lateinit var tokenPref: TokenPref
    private lateinit var token: Token

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentGlobalQuotesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            recyclerviewGlobalQuotes.layoutManager = LinearLayoutManager(activity)
            tokenPref = TokenPref(requireActivity())
            token = tokenPref.getToken()
            adapter = QuoteAdapter(requireActivity())
            recyclerviewGlobalQuotes.adapter = adapter
            presenter = MainPresenter(this@GlobalQuotesFragment, CoroutineContextProvider())
            progressbar.visibility = View.VISIBLE
            presenter.getAllQuote(token.token)
            swiperefresh.setOnRefreshListener {
                progressbar.visibility = View.INVISIBLE
                presenter.getAllQuote(token.token)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getAllQuote(token.token)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun showMessage(message: String) {
        Toast.makeText(requireActivity(), message, Toast.LENGTH_SHORT).show()
    }

    override fun resultQuote(data: ArrayList<Quote>) {
        adapter.differ.submitList(data)
        binding.apply {
            progressbar.visibility = View.INVISIBLE
            swiperefresh.isRefreshing = false
        }
    }

    override fun resultLogin(data: Login) {}
}