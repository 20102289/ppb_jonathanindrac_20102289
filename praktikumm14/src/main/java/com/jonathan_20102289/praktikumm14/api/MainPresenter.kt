package com.jonathan_20102289.praktikumm14.api

import android.util.Log
import com.jonathan_20102289.praktikumm14.CoroutineContextProvider
import com.jonathan_20102289.praktikumm14.interfacee.MainView
import com.jonathan_20102289.praktikumm14.model.Login
import com.jonathan_20102289.praktikumm14.model.Message
import com.jonathan_20102289.praktikumm14.model.QuoteResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenter(
    private val view: MainView, private val context:
    CoroutineContextProvider = CoroutineContextProvider()
) {
    fun getMyQuotes(token: String?) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                ApiMain().services.getMyQuotes("Bearer $token").enqueue(object :
                    Callback<QuoteResponse> {
                    override fun onResponse(
                        call: Call<QuoteResponse>,
                        response: Response<QuoteResponse>
                    ) {
                        if (response.code() == 200) {
                            response.body()?.quotes?.let {
                                view.resultQuote(it)
                            }
                        }
                    }

                    override fun onFailure(call: Call<QuoteResponse>, t: Throwable) {
                        view.showMessage("Koneksi terputus")
                    }
                })
            } catch (_: Exception) {
            }
        }
    }
    fun getClassQuote(token: String?) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                ApiMain().services.getClassQuotes("Bearer $token").enqueue(object :
                    Callback<QuoteResponse> {
                    override fun onResponse(
                        call: Call<QuoteResponse>,
                        response: Response<QuoteResponse>
                    ) {
                        if (response.code() == 200) {
                            response.body()?.quotes?.let {
                                view.resultQuote(it)
                            }
                        }
                    }

                    override fun onFailure(call: Call<QuoteResponse>, t: Throwable) {
                        view.showMessage("Koneksi terputus")
                    }

                })
            } catch (_: Exception) {
            }
        }

    }

    fun getAllQuote(token: String?) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                ApiMain().services.getAllQuotes("Bearer $token").enqueue(object :
                    Callback<QuoteResponse> {
                    override fun onResponse(
                        call: Call<QuoteResponse>,
                        response: Response<QuoteResponse>
                    ) {
                        if (response.code() == 200) {
                            response.body()?.quotes?.let {
                                view.resultQuote(it)
                            }
                        }
                    }

                    override fun onFailure(call: Call<QuoteResponse>, t: Throwable) {
                        view.showMessage("Koneksi terputus")
                    }

                })
            } catch (_: Exception) {
            }
        }

    }

    fun addQuote(token: String, title: String, description: String) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                ApiMain().services.addQuote("Bearer $token", title, description).enqueue(object :
                    Callback<Message> {
                    override fun onResponse(call: Call<Message>, response: Response<Message>) {
                        if (response.code() == 200) {
                            response.body()?.message?.let {
                                view.showMessage(it)
                            }
                        }
                    }

                    override fun onFailure(call: Call<Message>, t: Throwable) {
                        view.showMessage("Koneksi terputus")
                    }

                })
            } catch (_: Exception) {
            }
        }
    }

    fun updateQuote(token: String, quote_id: String, title: String, description: String) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                ApiMain().services.updateQuote("Bearer $token", quote_id, title, description)
                    .enqueue(object :
                        Callback<Message> {
                        override fun onResponse(call: Call<Message>, response: Response<Message>) {
                            if (response.code() == 200) {
                                response.body()?.message?.let {
                                    view.showMessage(it)
                                }
                            }
                        }

                        override fun onFailure(call: Call<Message>, t: Throwable) {
                            view.showMessage("Koneksi terputus")
                        }
                    })
            } catch (_: Exception) {
            }
        }

    }

    fun deleteQuote(token: String, quote_id: String) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                ApiMain().services.deleteQuote("Bearer $token", quote_id).enqueue(object :
                    Callback<Message> {
                    override fun onResponse(call: Call<Message>, response: Response<Message>) {
                        if (response.code() == 200) {
                            response.body()?.message?.let {
                                view.showMessage(it)
                            }
                        }
                    }

                    override fun onFailure(call: Call<Message>, t: Throwable) {
                        view.showMessage("Koneksi terputus")
                    }

                })
            } catch (e: Exception) {
            }
        }
    }

    fun login(nim: String, password: String) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                ApiMain().services.login(nim, password).enqueue(object : Callback<Login> {
                    override fun onResponse(call: Call<Login>, response: Response<Login>) {
                        if (response.code() == 200) {
                            response.body()?.let {
                                view.resultLogin(it)
                                view.showMessage("Login berhasil")
                            }
                        } else {
                            view.showMessage("Login gagal")
                        }
                    }

                    override fun onFailure(call: Call<Login>, t: Throwable) {
                        view.showMessage("Koneksi terputus")
                    }
                })
            } catch (e: Exception) {
                Log.d("TAG", e.message.toString())
            }
        }
    }
}