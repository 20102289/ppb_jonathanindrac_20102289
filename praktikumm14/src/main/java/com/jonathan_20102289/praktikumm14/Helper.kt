package com.jonathan_20102289.praktikumm14

object Helper {
    const val EXTRA_QUOTE = "extra_quote"
    const val EXTRA_POSITION = "extra_position"
    const val RESULT_ADD = 101
    const val REQUEST_UPDATE = 201
    const val ALERT_DIALOG_CLOSE = 10
    const val ALERT_DIALOG_DELETE = 20
}