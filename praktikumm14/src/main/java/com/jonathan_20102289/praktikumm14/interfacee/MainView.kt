package com.jonathan_20102289.praktikumm14.interfacee

import com.jonathan_20102289.praktikumm14.model.Login
import com.jonathan_20102289.praktikumm14.model.Quote

interface MainView {
    fun showMessage(message: String)
    fun resultQuote(data: ArrayList<Quote>)
    fun resultLogin(data: Login)
}