package com.jonathan_20102289.praktikum8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jonathan_20102289.praktikum8.databinding.ActivitySmsRecieverBinding

class SmsRecieverActivity : AppCompatActivity() {

    private val binding: ActivitySmsRecieverBinding by lazy {
        ActivitySmsRecieverBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.apply {
            title = getString(R.string.incoming_message)
            val senderNo = intent.getStringExtra(EXTRA_SMS_NO)
            val senderMessage = intent.getStringExtra(EXTRA_SMS_MESSAGE)
            tvFrom.text = getString(R.string.coming_from, senderNo)
            tvMessage.text = senderMessage
            val isDetect = intent.getBooleanExtra(EXTRA_SMS_BAN, false)
            tvDetect.text = if (isDetect) "Spam Detected" else ""

            btnClose.setOnClickListener { finish() }
        }

    }

    companion object {
        const val EXTRA_SMS_NO = "extra_sms_no"
        const val EXTRA_SMS_MESSAGE = "extra_sms_message"

        const val EXTRA_SMS_BAN = "extra_sms_ban"
    }
}