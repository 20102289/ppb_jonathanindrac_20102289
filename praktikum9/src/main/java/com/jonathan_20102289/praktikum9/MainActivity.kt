package com.jonathan_20102289.praktikum9

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.view.Menu
import android.view.MenuItem
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.jonathan_20102289.praktikum9.R
import com.jonathan_20102289.praktikum9.SettingModel
import com.jonathan_20102289.praktikum9.SettingPreference
import com.jonathan_20102289.praktikum9.SettingPreferenceActivity
import com.jonathan_20102289.praktikum9.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }
    private lateinit var settingModel: SettingModel
    private lateinit var mSettingPreference: SettingPreference


    private val resultLauncher: ActivityResultLauncher<Intent> =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.data != null && result.resultCode == SettingPreferenceActivity.RESULT_CODE) {
                settingModel =
                    result.data?.parcelable<SettingModel>(SettingPreferenceActivity.EXTRA_RESULT) as SettingModel
                populateView(settingModel)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        supportActionBar?.title = getString(R.string.main_title)
        mSettingPreference = SettingPreference(this)

//        binding.btnSetting.setOnClickListener {
//            val intent = Intent(this, SettingPreferenceActivity::class.java)
//            intent.putExtra("SETTING", settingModel)
//            resultLauncher.launch(intent)
//        }

        showExistingPreferences()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this, SettingPreferenceActivity::class.java)
                intent.putExtra("SETTING", settingModel)
                resultLauncher.launch(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private inline fun <reified T : Parcelable> Intent.parcelable(key: String): T? = when {
        Build.VERSION.SDK_INT >= 33 -> getParcelableExtra(key, T::class.java)
        else -> @Suppress("DEPRECATION") getParcelableExtra(key) as? T
    }

    private fun showExistingPreferences() {
        settingModel = mSettingPreference.getSetting()
        populateView(settingModel)
    }

    private fun populateView(settingModel: SettingModel) {
        with(binding) {
            tvName.text = settingModel.name.toString().ifEmpty { getString(R.string.empty_message) }
            tvNim.text = settingModel.nim.toString().ifEmpty { getString(R.string.empty_message) }
            tvGender.text =
                if (settingModel.gender) getString(R.string.laki) else getString(R.string.perempuan)
            tvEmail.text =
                settingModel.email.toString().ifEmpty { getString(R.string.empty_message) }
            tvPhone.text =
                settingModel.phoneNumber.toString().ifEmpty { getString(R.string.empty_message) }
            tvAge.text = settingModel.age.toString().ifEmpty { getString(R.string.empty_message) }
            tvTheme.text =
                if (settingModel.isDarkTheme) getString(R.string.dark) else getString(R.string.light)
        }
        if (settingModel.isDarkTheme) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            delegate.applyDayNight()
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            delegate.applyDayNight()
        }
    }
}