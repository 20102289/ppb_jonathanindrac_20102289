package com.jonathan_20102289.practice5

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jonathan_20102289.practice5.databinding.FragmentPractice5FirstBinding
import com.jonathan_20102289.practice5.databinding.FragmentPractice5ReadDataBinding

class Practice5ReadDataFragment : Fragment() {

    private var _binding: FragmentPractice5ReadDataBinding? = null
    private val binding get() = _binding
    var nim : Int? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPractice5ReadDataBinding.inflate(inflater, container, false)
        return binding?.root

    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        binding?.apply {
            if (arguments!=null){
                val myName =arguments?.getString(EXTRA_NAMA)
                tvMyData.text= "Nama Saya : $myName, NIM Saya : $nim"
            }
            btnKembaliBeranda.setOnClickListener {
                val mIntent = Intent(activity, Practice5activity::class.java)
                startActivity(mIntent)
            }
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    companion object{
        var EXTRA_NAMA = "Extra_Nama"
    }
}