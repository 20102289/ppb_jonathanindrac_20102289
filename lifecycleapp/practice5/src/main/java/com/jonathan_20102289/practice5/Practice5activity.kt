package com.jonathan_20102289.practice5

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.jonathan_20102289.practice5.databinding.ActivityPractice5activityBinding

class Practice5activity : AppCompatActivity() {
//     Binding Practice5
    private val binding: ActivityPractice5activityBinding by lazy {
        ActivityPractice5activityBinding.inflate(layoutInflater)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root) // Binding root

//        Fungsi Btn Prodi
        binding.apply {
            btnProdi.setOnClickListener {
                val namaProdi = inputProdi.text.toString()
                if (namaProdi.isEmpty()) {
                    inputProdi.error = "Prodi Tidak Boleh Kosong"
                    return@setOnClickListener
                }
                val moveWithDataIntent =
                    Intent(this@Practice5activity, Practice5ReadDataActivity::class.java)
                moveWithDataIntent.putExtra(Practice5ReadDataActivity.EXTRA_PRODI, namaProdi)
                startActivity(moveWithDataIntent)
            }
//            Fungsi On Create
            btnCallBrowser.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("http://ittelkom-pwt.ac.id/")
                startActivity(intent)
            }
            btnCallCamera.setOnClickListener {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivity(intent)
            }
            btnCallPhone.setOnClickListener {
                val phoneNumber = inputPhoneNumber.text.toString()
                if (phoneNumber.isEmpty()) {
                    inputPhoneNumber.error = "Nomor Telpon Tidak Boleh Kosong"
                    return@setOnClickListener
                }
                val intent = Intent(Intent.ACTION_CALL)
                intent.data = Uri.parse("tel:$phoneNumber")
                startActivity(intent)
            }
            btnFragment.setOnClickListener{
                val intent = Intent(this@Practice5activity, Practice5ForFragmentActivity::class.java)
                startActivity(intent)
            }
        }
//        Panggil Oncreate
        setupPermissions()
    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
            android.Manifest.permission.CALL_PHONE)
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.CALL_PHONE),
                CALL_REQUEST_CODE)
        }
    }
//    Fungsi Companion Object
    companion object{
        const val CALL_REQUEST_CODE = 100


    }
}
