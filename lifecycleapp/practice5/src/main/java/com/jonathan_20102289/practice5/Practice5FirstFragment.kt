package com.jonathan_20102289.practice5

import android.os.Bundle
import android.support.v4.os.IResultReceiver._Parcel
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jonathan_20102289.practice5.databinding.FragmentPractice5FirstBinding

class Practice5FirstFragment : Fragment() {

    private var _binding: FragmentPractice5FirstBinding? = null
    private val binding get()=_binding

    override fun onCreateView(
        inflater: LayoutInflater, container : ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPractice5FirstBinding.inflate(inflater, container, false)
        return binding?.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            btnToSecondFragment.setOnClickListener{
                val namaSaya = inputNamaSaya.text.toString()
                if (namaSaya.isEmpty()) {
                    inputNamaSaya.error = "Nama Tidak Boleh Kosong"
                    return@setOnClickListener
                }
                val nimSaya = inputNimSaya.text.toString()
                if (nimSaya.isEmpty()) {
                    inputNimSaya.error = "Nim Tidak Boleh Kosong"
                    return@setOnClickListener
                }
                val mReadDataFragment = Practice5ReadDataFragment()
                val mBundle = Bundle()
                mBundle.putString(Practice5ReadDataFragment.EXTRA_NAMA, namaSaya)
                mReadDataFragment.arguments = mBundle
                mReadDataFragment.nim = nimSaya.toInt()

                val mFragmentManager = parentFragmentManager
                mFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_container, mReadDataFragment, Practice5ReadDataFragment::class.java.simpleName)
                    .addToBackStack(null)
                    .commit()
            }
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}