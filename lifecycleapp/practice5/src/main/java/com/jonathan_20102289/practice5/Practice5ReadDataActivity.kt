package com.jonathan_20102289.practice5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jonathan_20102289.practice5.databinding.ActivityPractice5ReadDataBinding
import com.jonathan_20102289.practice5.databinding.ActivityPractice5activityBinding

class Practice5ReadDataActivity : AppCompatActivity() {
    private val binding: ActivityPractice5ReadDataBinding by lazy {
        ActivityPractice5ReadDataBinding.inflate(layoutInflater)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        // Binding.lblProdiSaya
        val prodi = intent.getStringExtra(EXTRA_PRODI)
        binding.lblProdiSaya.text = "Prodi Saya Adalah $prodi"
    }
//     Companion Object
    companion object {
        const val EXTRA_PRODI = "extra_prodi"
    }
}