package com.jonathan_20102289.praktikum6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.jonathan_20102289.praktikum6.adapter.CardViewMyDataAdapter
import com.jonathan_20102289.praktikum6.adapter.GridMyDataAdapter
import com.jonathan_20102289.praktikum6.adapter.ListMyDataAdapter
import com.jonathan_20102289.praktikum6.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val binding: ActivityMainBinding by lazy{
        ActivityMainBinding.inflate(layoutInflater)
    }

    // List showRecyclerList
    private val list = ArrayList<MyData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.rvMydata.setHasFixedSize(true)
        list.addAll(getListMyDatas())
        showRecyclerList()
    }

    private fun showRecyclerList() {
        binding.apply {
            rvMydata.layoutManager = LinearLayoutManager(this@MainActivity)
            val listMyDataAdapter = ListMyDataAdapter(list)
            rvMydata.adapter = listMyDataAdapter
        }

    }

    private fun showRecyclerGrid() {
        binding.apply {
            rvMydata.layoutManager = GridLayoutManager(this@MainActivity, 2)
            val gridMyDataAdapter = GridMyDataAdapter(list)
            rvMydata.adapter = gridMyDataAdapter
        }
    }

    private fun showRecyclerCardView() {
        binding.apply {
            rvMydata.layoutManager = LinearLayoutManager(this@MainActivity)
            val cardViewMyDataAdapter = CardViewMyDataAdapter (list)
            rvMydata.adapter = cardViewMyDataAdapter

        }
    }

    private fun getListMyDatas(): ArrayList<MyData> {
        val dataName = resources.getStringArray(R.array.data_name)
        val dataDescription = resources.getStringArray(R.array.data_description)
        val dataPhoto = resources.getStringArray(R.array.data_photo)
        val dataLat = resources.getStringArray(R.array.data_lat)
        val dataLang = resources.getStringArray(R.array.data_lang)
        val listMyData = ArrayList<MyData>()
        for (position in dataName.indices) {
            val myData = MyData(
                dataName[position],
                dataDescription[position],
                dataPhoto[position],
                dataLat[position].toDouble(),
                dataLang[position].toDouble()
            )
            listMyData.add(myData)
        }
        return listMyData
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        setMode(item.itemId)
        return super.onOptionsItemSelected(item)
    }
    private fun setMode(selectedMode: Int) {
        when (selectedMode) {
            R.id.action_list -> showRecyclerList()
            R.id.action_grid -> showRecyclerGrid()
            R.id.action_cardview -> showRecyclerCardView()

        }
    }

}