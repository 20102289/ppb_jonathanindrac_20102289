package com.jonathan_20102289.praktikum6

import android.app.Activity
import android.content.Intent
import android.os.Build.VERSION.SDK_INT
import android.os.Bundle
import android.os.Parcelable
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.jonathan_20102289.praktikum6.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val myData = parcelable<MyData>(EXTRA_MY_DATA)

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.apply {
            title = myData?.name.toString()
            setDisplayHomeAsUpEnabled(true)
        }

        binding.apply {
            toolbarLayout.title = myData?.name.toString()
            fab.setOnClickListener { view ->
                val moveWithObjectIntent = Intent(this@DetailActivity, MapsActivity::class.java)
                moveWithObjectIntent.putExtra(MapsActivity.EXTRA_MY_DATA, myData)
                startActivity(moveWithObjectIntent)

            }
            contentScrolling.tvDetailDescription.text = myData?.description.toString()
            Glide.with(this@DetailActivity)
                .load(myData?.photo.toString())
                .apply(RequestOptions().override(700, 700))
                .into(ivDetailPhoto)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    companion object {
        const val EXTRA_MY_DATA = "extra_my_data"
    }
    private inline fun <reified T : Parcelable> Activity.parcelable(key: String): T? = when {
        SDK_INT >= 33 -> intent.getParcelableExtra(key, T::class.java)
        else -> @Suppress("DEPRECATION") intent.getParcelableExtra(key) as? T
    }

}
